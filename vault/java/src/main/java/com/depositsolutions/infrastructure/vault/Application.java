package com.depositsolutions.infrastructure.vault;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@SpringBootApplication
@Configuration
public class Application {
    private static Logger LOG = LoggerFactory.getLogger(Application.class.getName());

    @Value("${name}")
    String name;

    public Application() {
        LOG.info(String.format("Value in vault inside constructor is: %s", name));
    }

    @PostConstruct
    public void logValue() {
        LOG.info(String.format("Value in vault after constructor is: %s", name));
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
