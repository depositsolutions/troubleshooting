package com.depositsolutions.infrastructure;

import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

public class RabbitMQTester {
    private final static Logger LOGGER = LoggerFactory.getLogger(RabbitMQTester.class);

    public static void main(String[] args) throws NoSuchAlgorithmException, KeyManagementException, URISyntaxException, IOException, TimeoutException, KeyStoreException {
        if (args.length < 2) {
            LOGGER.error("Usage: amqp[s]://<user>:<password>@server[:port] <queue_name>");
            System.exit(1);
        }
        String uri = args[0];
        String queue = args[1];
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri(uri);
        LOGGER.info(String.format("Connecting to queue %s at the provided URI...", queue, uri));
        final Connection connection = factory.newConnection();
        final Channel channel = connection.createChannel();
        channel.queueDeclare(queue, true, false, false, null);
        String message = "Test message from Java";
        channel.basicPublish("", queue, null, message.getBytes());
        LOGGER.info(String.format("Message posted to queue: %s", message));
        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body)
                    throws IOException {
                String message = new String(body, "UTF-8");
                LOGGER.info(String.format("Received message form queue: %s", message));
                LOGGER.info("Ending test run");
                try {
                    channel.close();
                } catch (TimeoutException e) {
                    LOGGER.error("Timeout when closing channel", e);
                }
                connection.close();
            }
        };
        LOGGER.info(String.format("Consuming messages from queue %s", queue));
        channel.basicConsume(queue, true, consumer);
    }
}
